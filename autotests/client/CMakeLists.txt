
include_directories(../../src/client)

macro(windowmetadata_add_test)
    foreach(_testname ${ARGN})
        set(test_SRCS ${_testname}.cpp)
        ecm_qt_declare_logging_category(test_SRCS HEADER debug.h
                                               IDENTIFIER WMD
                                               CATEGORY_NAME kde.windowmetadata
                                               DEFAULT_SEVERITY Info)
        add_executable(${_testname} ${test_SRCS})
        target_link_libraries(${_testname} Qt5::Core Qt5::Gui Qt5::Test Qt5::DBus KF5::WindowMetadataClient KF5::WindowMetadataServer)
        add_test(NAME ${_testname}
                 COMMAND $<TARGET_FILE:${_testname}>
        )
        ecm_mark_as_test(${_testname})
    endforeach(_testname)
endmacro(windowmetadata_add_test)


windowmetadata_add_test(test_windowmetadata_client)
