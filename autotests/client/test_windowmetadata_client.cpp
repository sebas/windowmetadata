/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/
// Qt
#include <QtTest/QtTest>
#include <QTemporaryFile>

// Own
#include "metadataclient.h"
#include "debug.h"

// Server
#include "controller.h"
#include "metadataitem.h"
#include "servicewatcher_p.h"

static auto s_serviceBaseName = QStringLiteral("org.kde.test.kwin.windowmetadata1.test_metadataclient");

using namespace WindowMetadata::Client;
using namespace WindowMetadata::Server;


class TestWindowMetadata : public QObject
{
    Q_OBJECT
public:
    explicit TestWindowMetadata(QObject *parent = nullptr);
private Q_SLOTS:
    void init();
    void cleanup();

    void testCreate();
    void testRequestThumbnail();
    void testDirtyThumbnail();
    void testSetThumbnail();
    void testSendData();
    void testWidChange();
    void testDocument();
};

TestWindowMetadata::TestWindowMetadata(QObject *parent)
    : QObject(parent)
{
    qRegisterMetaType<WindowMetadata::Server::MetadataItemPointer>("MetadataItemPointer");
}

void TestWindowMetadata::init()
{
    //using namespace WindowMetadata::Client;
}

void TestWindowMetadata::cleanup()
{
    ServiceWatcher::shutdown();
}

void TestWindowMetadata::testCreate()
{
    using namespace WindowMetadata::Client;
    auto c1 = new MetadataClient(this);

    QSignalSpy regSpy(c1, &MetadataClient::registered);
    c1->registerService();

    QCOMPARE(regSpy.count(), 1);
    auto c2 = new MetadataClient(this);

    QSignalSpy regSpy2(c2, &MetadataClient::registered);
    c2->registerService();

    QCOMPARE(regSpy2.count(), 1);

    delete c1;
    delete c2;
}

void TestWindowMetadata::testRequestThumbnail()
{
    auto controller = new Controller(this, s_serviceBaseName);

    QSignalSpy addedSpy(controller, &Controller::itemAdded);
    QSignalSpy removedSpy(controller, &Controller::itemRemoved);

    auto c1 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    QSignalSpy thumbSpy(c1, &MetadataClient::thumbnailRequested);
    c1->registerService();
    QVERIFY(addedSpy.wait(200));
    QCOMPARE(controller->items().count(), 1);

    QSize thumbnailSize(40, 60);
    int wid = 1337;

    auto serveritem = controller->items().first();
    serveritem->requestThumbnail(thumbnailSize, wid);

    QVERIFY(thumbSpy.wait(200));

    auto c_size = thumbSpy.first().first().value<QSize>();
    auto c_wid = thumbSpy.first().at(1).value<int>();
    auto c_fd = thumbSpy.first().last().value<QString>();
    QCOMPARE(c_size, thumbnailSize);
    QVERIFY(!c_fd.isEmpty());
    QCOMPARE(c_wid, wid);

    delete c1;
    delete controller;
}

void TestWindowMetadata::testDirtyThumbnail()
{
    auto controller = new Controller(this, s_serviceBaseName);

    QSignalSpy addedSpy(controller, &Controller::itemAdded);
    QSignalSpy removedSpy(controller, &Controller::itemRemoved);

    auto c1 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    QSignalSpy thumbSpy(c1, &MetadataClient::thumbnailRequested);
    c1->registerService();
    QVERIFY(addedSpy.wait(200));
    QCOMPARE(controller->items().count(), 1);

    auto serveritem = controller->items().first();
    QSignalSpy serverSpy(serveritem.data(), &MetadataItem::thumbnailChanged);

    auto _size = QSize(40, 60);
    auto _wid = 7593;

    // We need to request a thumbnail at least once just to set the size internally
    serveritem->requestThumbnail(_size, _wid);
    QVERIFY(thumbSpy.wait(200));
    QCOMPARE(c1->requestedSize(), _size);

    // Now update our image and fire the dirty signal
    QImage _image(_size, QImage::Format_ARGB32);
    _image.fill(Qt::blue);
    c1->setDirty(_wid);

    // ... and wait for a reply request from the server
    QVERIFY(thumbSpy.wait(200));
    // Request is in, now send the updated thumbnail and make sure it arrives on the
    // server side
    c1->setThumbnail(_image, _wid);
    QVERIFY(serverSpy.wait(200));

    QCOMPARE(serveritem->wid(), _wid);
    QCOMPARE(serveritem->thumbnail().size(), _size);

    delete controller;
    delete c1;
}

void TestWindowMetadata::testSetThumbnail()
{
    QString thumbnailTestFile = QFINDTESTDATA("../../tests/client/bluegreen.png");
    qCDebug(WMD) << "Test file: " << thumbnailTestFile;
    QImage thumbnailSource(thumbnailTestFile);

    QSize thumbnailSize(96, 64); // conveniently, this is the exact size of above png image
    int wid = 1337;

    auto controller = new Controller(this, s_serviceBaseName);

    QSignalSpy addedSpy(controller, &Controller::itemAdded);
    QSignalSpy removedSpy(controller, &Controller::itemRemoved);

    auto c1 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    QSignalSpy thumbSpy(c1, &MetadataClient::thumbnailRequested);
    c1->registerService();
    QVERIFY(addedSpy.wait(200));
    QCOMPARE(controller->items().count(), 1);


    auto serveritem = controller->items().first();
    serveritem->requestThumbnail(thumbnailSize, wid);

    QVERIFY(thumbSpy.wait(200));

    auto c_size = thumbSpy.first().first().value<QSize>();
    auto c_wid = thumbSpy.first().at(1).value<int>();
    auto c_fd = thumbSpy.first().last().value<QString>();
    QCOMPARE(c_size, thumbnailSize);
    QVERIFY(!c_fd.isEmpty());
    QCOMPARE(c_wid, wid);
    QCOMPARE(c_size, thumbnailSource.size());

    // The server has indicated that it wants a thumbnail, set it
    QSignalSpy thumbChangedSpy(serveritem.data(), &MetadataItem::thumbnailChanged);
    c1->setThumbnail(thumbnailSource);
    QVERIFY(thumbChangedSpy.wait(200));

    auto serverthumb = serveritem->thumbnail();
    QVERIFY(!serverthumb.isNull());
    QCOMPARE(serverthumb.size(), thumbnailSize);

    // Let's try again, with a different size

    thumbnailSize = thumbnailSize * 2;
    serveritem->requestThumbnail(thumbnailSize, wid);

    QVERIFY(thumbSpy.wait(200));

    c_size = thumbSpy.at(1).first().value<QSize>();
    c_wid = thumbSpy.at(1).at(1).value<int>();
    c_fd = thumbSpy.at(1).last().value<QString>();

    QCOMPARE(c_size, thumbnailSize);
    QVERIFY(!c_fd.isEmpty());
    QCOMPARE(c_wid, wid);

    // The server has indicated that it wants a thumbnail, resize our copy as cheap-ass test
    auto thumbnailSource2 = thumbnailSource.scaled(thumbnailSize);

    QCOMPARE(c_size, thumbnailSource2.size());
    c1->setThumbnail(thumbnailSource2);

    QVERIFY(thumbChangedSpy.wait(200));

    serverthumb = serveritem->thumbnail();
    QVERIFY(!serverthumb.isNull());
    QCOMPARE(serverthumb.size(), thumbnailSize);

    delete c1;
    delete controller;
}

void TestWindowMetadata::testSendData()
{
    auto controller = new Controller(this, s_serviceBaseName);

    QSignalSpy addedSpy(controller, &Controller::itemAdded);
    QSignalSpy removedSpy(controller, &Controller::itemRemoved);

    auto c1 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    auto pid = qApp->applicationPid();
    //QSignalSpy thumbSpy(c1, &MetadataClient::thumbnailRequested);
    c1->registerService();
    QVERIFY(addedSpy.wait(200));
    QCOMPARE(controller->items().count(), 1);

    auto serveritem = controller->items().first();
    QSignalSpy serverSpy(serveritem.data(), &MetadataItem::titleChanged);

    auto new_t = QStringLiteral("NEW TITLE");
    c1->setTitle(new_t);

    QVERIFY(serverSpy.wait(200));

    QCOMPARE(new_t, serveritem->title());
    QCOMPARE(pid, serveritem->pid());
    qCDebug(WMD) << "title:" << new_t << serveritem->title();

    delete controller;
    delete c1;
}

void TestWindowMetadata::testWidChange()
{
    auto controller = new Controller(this, s_serviceBaseName);
    QSignalSpy addedSpy(controller, &Controller::itemAdded);

    auto c1 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    QSignalSpy clientSpy(c1, &MetadataClient::widChanged);
    auto old_wid = INT_MAX;
    auto new_wid = 666;

    c1->setWid(old_wid);
    QCOMPARE(clientSpy.count(), 1);
    c1->registerService();
    QVERIFY(addedSpy.wait(200));
    QCOMPARE(controller->items().count(), 1);

    QCOMPARE(c1->wid(), old_wid);
    auto serveritem = controller->items().first();
    QCOMPARE(serveritem->wid(), old_wid);

    QSignalSpy serverSpy(serveritem.data(), &MetadataItem::widChanged);

    c1->setWid(new_wid);

    QVERIFY(serverSpy.wait(200));
    QCOMPARE(serverSpy.count(), 1);
    QCOMPARE(clientSpy.count(), 2);
    QCOMPARE(new_wid, serveritem->wid());

    delete controller;
    delete c1;
}

void TestWindowMetadata::testDocument()
{
    auto controller = new Controller(this, s_serviceBaseName);

    QSignalSpy addedSpy(controller, &Controller::itemAdded);
    QSignalSpy removedSpy(controller, &Controller::itemRemoved);

    auto c1 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    auto pid = qApp->applicationPid();

    c1->registerService();
    QVERIFY(addedSpy.wait(200));
    QCOMPARE(controller->items().count(), 1);

    auto serveritem = controller->items().first();
    QSignalSpy serverSpy(serveritem.data(), &MetadataItem::documentChanged);

    auto new_t = QStringLiteral("file://home/sebas/Documents/Suunto_D4i_UserGuide_EN.pdf");
    c1->setDocument(new_t);

    QVERIFY(serverSpy.wait(200));

    QCOMPARE(new_t, serveritem->document());
    QCOMPARE(pid, serveritem->pid());
    qCDebug(WMD) << "title:" << new_t << serveritem->document();

    delete controller;
    delete c1;
}


QTEST_GUILESS_MAIN(TestWindowMetadata)
#include "test_windowmetadata_client.moc"
