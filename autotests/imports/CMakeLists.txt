
include_directories(../../src/client)

macro(windowmetadata_add_declarative_test)
    foreach(_testname ${ARGN})
        set(test_SRCS ${_testname}.cpp)
        ecm_qt_declare_logging_category(test_SRCS HEADER debug.h
                                               IDENTIFIER WMD
                                               CATEGORY_NAME kde.windowmetadata
                                               DEFAULT_SEVERITY Info)
        add_executable(${_testname} ${test_SRCS})
        target_link_libraries(${_testname}
                                Qt5::Core
                                Qt5::Gui
                                Qt5::Test
                                Qt5::DBus
                                Qt5::Qml
                                Qt5::Quick
                                KF5::WindowMetadataClient
                                KF5::WindowMetadataServer)
        add_test(NAME ${_testname}
                 COMMAND $<TARGET_FILE:${_testname}>
        )
        ecm_mark_as_test(${_testname})
    endforeach(_testname)
endmacro(windowmetadata_add_declarative_test)


windowmetadata_add_declarative_test(test_server_import)
windowmetadata_add_declarative_test(test_client_import)
