/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/
// Qt
#include <QtTest/QtTest>
#include <QTemporaryFile>

#include <QtQml>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickItem>
#include <QQuickView>

// Own
#include "metadataclient.h"
#include "debug.h"

// Server
#include "controller.h"
#include "metadataitem.h"
#include "servicewatcher_p.h"

static auto s_serviceBaseName = QStringLiteral("org.kde.test.kwin.windowmetadata1.test_metadata_imports"); // probably needs support in the declarative classes

using namespace WindowMetadata::Client;
using namespace WindowMetadata::Server;


class TestClientImport : public QObject
{
    Q_OBJECT
public:
    explicit TestClientImport(QObject *parent = nullptr);
private Q_SLOTS:
    void init();
    void cleanup();

    void loadClient();

Q_SIGNALS:
    void clientReady();
    void clientError();

private:
    QQuickView* m_clientView = nullptr;
    QObject* m_clientObject = nullptr;
};

TestClientImport::TestClientImport(QObject *parent)
    : QObject(parent)
{
    //qRegisterMetaType<WindowMetadata::Server::MetadataItemPointer>("MetadataItemPointer");
}

void TestClientImport::init()
{
    m_clientView = new QQuickView();
    connect(m_clientView, &QQuickView::statusChanged, this,
        [=] (QQuickView::Status status) {
            if (status == QQuickView::Ready) {
                Q_EMIT clientReady();
            } else if (status == QQuickView::Error) {
                Q_EMIT clientError();
            }
        }
    );
}

void TestClientImport::cleanup()
{
    ServiceWatcher::shutdown();
}

void TestClientImport::loadClient()
{
    m_clientView->setResizeMode(QQuickView::SizeRootObjectToView);
    //QQuickWindow::setDefaultAlphaBuffer(true);
    QSignalSpy loadedSpy(this, &TestClientImport::clientReady);
    const QString qmlFile = QFINDTESTDATA("testclient.qml");
    m_clientView->setSource(QUrl::fromLocalFile(qmlFile));
    m_clientView->show();
    QCOMPARE(loadedSpy.count(), 1);

    auto rootItem = m_clientView->rootObject();
    QVERIFY(rootItem != nullptr);

    m_clientObject = rootItem->findChild<QObject*>("clientObject");
    QVERIFY(m_clientObject != nullptr);

    auto registered = rootItem->property("registered").toBool();
    QCOMPARE(registered, false);

    QSignalSpy registeredSpy(m_clientObject, SIGNAL(registeredChanged()));
    QVERIFY(registeredSpy.isValid());

    QSignalSpy thumbnailSpy(m_clientObject, SIGNAL(thumbnailChanged()));
    QVERIFY(thumbnailSpy.isValid());

    QSignalSpy documentSpy(m_clientObject, SIGNAL(documentChanged()));
    QVERIFY(documentSpy.isValid());

    QSignalSpy titleSpy(m_clientObject, SIGNAL(titleChanged()));
    QVERIFY(titleSpy.isValid());

    QVariant returnedValue;
    QVariant msg = "Hello from C++";
    QMetaObject::invokeMethod(rootItem, "testRegister",
            Q_RETURN_ARG(QVariant, returnedValue),
            Q_ARG(QVariant, msg));
    QVERIFY(returnedValue.toBool());
    QCOMPARE(registeredSpy.count(), 1);

    // title
    auto bratwurscht = QStringLiteral("Brätwürßt€");
    QCOMPARE(m_clientObject->property("title").toString(), bratwurscht);
    auto pizza = QStringLiteral("Pizza Funghi");
    m_clientObject->setProperty("title", pizza);
    QCOMPARE(m_clientObject->property("title").toString(), pizza);
    QCOMPARE(titleSpy.count(), 1);

    QMetaObject::invokeMethod(rootItem, "testTitle",
            Q_RETURN_ARG(QVariant, returnedValue),
            Q_ARG(QVariant, msg));
    //QVERIFY(titleSpy.wait(200));
    QCOMPARE(titleSpy.count(), 2);
    QCOMPARE(m_clientObject->property("title"), msg);

    QVERIFY(returnedValue.toBool());

    // doc
    auto suunto_pdf = QStringLiteral("file://home/sebas/Documents/Suunto_D4i_UserGuide_EN.pdf");
    QCOMPARE(m_clientObject->property("document").toString(), QStringLiteral("testimage4.png"));
    auto pizza_funghi = QStringLiteral("/pizza/funghi");
    m_clientObject->setProperty("document", pizza_funghi);
    QCOMPARE(m_clientObject->property("document").toString(), pizza_funghi);
    QCOMPARE(documentSpy.count(), 1);

    QMetaObject::invokeMethod(rootItem, "testDocument",
            Q_RETURN_ARG(QVariant, returnedValue),
            Q_ARG(QVariant, suunto_pdf));
    QCOMPARE(documentSpy.count(), 2);
    QCOMPARE(m_clientObject->property("document").toString(), suunto_pdf);

    QVERIFY(returnedValue.toBool());

    // thumbnail
    QCOMPARE(thumbnailSpy.count(), 0);


}


QTEST_MAIN(TestClientImport)
#include "test_client_import.moc"
