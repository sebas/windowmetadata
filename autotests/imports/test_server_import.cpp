/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/
// Qt
#include <QtTest/QtTest>
#include <QTemporaryFile>

#include <QtQml>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickItem>
#include <QQuickView>

// Own
#include "metadataclient.h"
#include "debug.h"

// Server
#include "controller.h"
#include "metadataitem.h"
#include "servicewatcher_p.h"

static auto s_serviceBaseName = QStringLiteral("org.kde.test.kwin.windowmetadata1.test_metadata_server_imports"); // probably needs support in the declarative classes

using namespace WindowMetadata::Client;
using namespace WindowMetadata::Server;


class TestImports : public QObject
{
    Q_OBJECT
public:
    explicit TestImports(QObject *parent = nullptr);
private Q_SLOTS:
    void init();
    void cleanup();

    void loadServer();
    void addClient();

Q_SIGNALS:
    void serverReady();
    void serverError();

private:
    QQuickView* m_serverView = nullptr;

    QObject* m_controllerObject = nullptr;
};

TestImports::TestImports(QObject *parent)
    : QObject(parent)
{
    qputenv("WINDOWMETADATA_SERVICEBASENAME", s_serviceBaseName.toLatin1());
    qRegisterMetaType<WindowMetadata::Server::MetadataItemPointer>("MetadataItemPointer");
}

void TestImports::init()
{
    m_serverView = new QQuickView();
    connect(m_serverView, &QQuickView::statusChanged, this,
        [=] (QQuickView::Status status) {
            if (status == QQuickView::Ready) {
                Q_EMIT serverReady();
            } else if (status == QQuickView::Error) {
                Q_EMIT serverError();
            }
        }
    );
}

void TestImports::cleanup()
{
    ServiceWatcher::shutdown();
}

void TestImports::loadServer()
{
    m_serverView->setResizeMode(QQuickView::SizeRootObjectToView);
    //QQuickWindow::setDefaultAlphaBuffer(true);
    QSignalSpy loadedSpy(this, &TestImports::serverReady);
    const QString qmlFile = QFINDTESTDATA("testserver.qml");
    m_serverView->setSource(QUrl::fromLocalFile(qmlFile));
    m_serverView->show();
    QCOMPARE(loadedSpy.count(), 1);

    auto rootItem = m_serverView->rootObject();
    QVERIFY(rootItem != nullptr);

    m_controllerObject = rootItem->findChild<QObject*>("controllerObject");
    QVERIFY(m_controllerObject != nullptr);
//     QSignalSpy titleSpy(m_clientObject, SIGNAL(titleChanged()));
//     QVERIFY(titleSpy.isValid());
//
//     QVariant returnedValue;
//     QVariant msg = "Hello from C++";
//     QMetaObject::invokeMethod(rootItem, "testRegister",
//             Q_RETURN_ARG(QVariant, returnedValue),
//             Q_ARG(QVariant, msg));
//     QVERIFY(returnedValue.toBool());
//     QCOMPARE(registeredSpy.count(), 1);

}

void TestImports::addClient()
{
    qRegisterMetaType<MetadataItemPointer>("MetadataItemPointer");
    auto controller = new Controller(this, s_serviceBaseName);

    //auto controller = new Controller(this, s_serviceBaseName);
    QSignalSpy addedSpy(controller, SIGNAL(itemAdded(MetadataItemPointer)));
    QSignalSpy removedSpy(m_controllerObject, SIGNAL(itemRemoved(MetadataItemPointer)));
    QSignalSpy countSpy(m_controllerObject, SIGNAL(clientCountChanged()));

    auto c1 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    QSignalSpy clientSpy(c1, &MetadataClient::widChanged);
    auto old_wid = INT_MAX;
    auto new_wid = 666;

    c1->setWid(old_wid);
    QCOMPARE(clientSpy.count(), 1);
    c1->registerService();

    QVERIFY(addedSpy.wait(3000));
    //QVERIFY(countSpy.count() == 1);
    //QCOMPARE(m_controllerObject->property("item").toList().count(), 1);

    QCOMPARE(c1->wid(), old_wid);
//     auto serveritem = controller->items().first();
//     QCOMPARE(serveritem->wid(), old_wid);
//
//     QSignalSpy serverSpy(serveritem.data(), &MetadataItem::widChanged);

    c1->setWid(new_wid);

//     QVERIFY(serverSpy.wait(200));
//     QCOMPARE(serverSpy.count(), 1);
    QCOMPARE(clientSpy.count(), 2);
//     QCOMPARE(new_wid, serveritem->wid());
//
//     delete controller;
    delete c1;
}


QTEST_MAIN(TestImports)
#include "test_server_import.moc"
