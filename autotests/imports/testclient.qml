/*
*   Copyright 2016 by Sebastian Kügler <sebas@kde.org>
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU Library General Public License as
*   published by the Free Software Foundation; either version 2, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Library General Public License for more details
*
*   You should have received a copy of the GNU Library General Public
*   License along with this program; if not, write to the
*   Free Software Foundation, Inc.,
*   51 Franklin Street, Fifth Floor, Boston, MA  2.010-1301, USA.
*/

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2

import org.kde.kwin.windowmetadata.client 1.0

Rectangle {
    id: root
    opacity: 0.8
    color: "lightgrey"
    width: 600
    height: 400

    MetadataClient {
        id: client
        objectName: "clientObject"
        document: "testimage4.png"
        title: "Brätwürßt€"
        thumbnail: Rectangle {
            id: previewItem
            anchors.fill: parent
            color: "lightblue"

            Rectangle {
                id: overlayBackground
                height: overlaytext.paintedHeight * 1.4
                color: "black"
                opacity: 0.7
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
            }
            Text {
                id: overlaytext
                color: "lightgrey"
                text: "Dimension: " + parent.width + " x " + parent.height
                elide: Text.ElideLeft
                anchors {
                    left: overlayBackground.left
                    leftMargin: overlaytext.paintedHeight / 2
                    right: overlayBackground.right
                    rightMargin: overlaytext.paintedHeight / 2
                    verticalCenter: overlayBackground.verticalCenter
                }
            }
        }
    }

    function testRegister(msg)
    {
        print("testRegister invoked: " + msg);
        client.registered = true;
        return true;
    }

    function testTitle(test_title)
    {
        client.title = test_title;
        client.setDirty(true);
        return true;
    }

    function testDocument(test_document)
    {
        client.document = test_document;
        client.setDirty(true);
        return true;
    }

    Component.onCompleted: {
        print("importtest.qml loaded.");
    }
}
