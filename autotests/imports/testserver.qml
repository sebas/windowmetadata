/*
*   Copyright 2016 by Sebastian Kügler <sebas@kde.org>
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU Library General Public License as
*   published by the Free Software Foundation; either version 2, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Library General Public License for more details
*
*   You should have received a copy of the GNU Library General Public
*   License along with this program; if not, write to the
*   Free Software Foundation, Inc.,
*   51 Franklin Street, Fifth Floor, Boston, MA  2.010-1301, USA.
*/

import QtQuick 2.5
import QtQuick.Controls 1.4

import org.kde.kwin.windowmetadata.server 1.0

Rectangle {
    id: root
    opacity: 0.8
    color: "lightgrey"
    width: 800
    height: 600


    Controller {
        id: controller
        objectName: "controllerObject"

        signal clientCountChanged();

        onClientsChanged: {

            var i = clients.length;
            print("Clients now: "  + i);
            clientCount();
        }
    }

    function itemCount()
    {
        return controller.clients.length
    }

    ListView {
        id: clientList
        anchors.fill: parent
        spacing: 10

        model: controller.clients
        delegate: Rectangle {

            property int delegateHeight : delegateWidth * 0.75
            property int delegateWidth : 128
            property int scaleFactor : scaleSlider.value

            width: parent.width
            height: delegateHeight * scaleFactor

            Image {
                id: thumbImage2
                anchors {
                    top: parent.top
                    //bottom: parent.bottom
                    left: parent.left
                }
                width: delegateWidth * scaleFactor
                height: delegateHeight * scaleFactor
                sourceSize.width: width
                sourceSize.height: height
                source: thumbnailUrl
//                 Behavior on height { PropertyAnimation { duration: 200 } }
//                 Behavior on width { PropertyAnimation { duration: 200 } }
            }
            Text {
                id: titleText
                anchors {
                    top: parent.top
                    bottom: widText.top
                    left: thumbImage2.right
                    right: parent.right
                    margins: 10
                }
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: "<b>Title:</b> " + title + "<br/><b>Document:</b>  " + document
                renderType: Text.RichText
            }

            Text {
                id: widText
                anchors {
//                     top: titleText.bottom
                    bottom: parent.bottom
                    left: thumbImage2.right
                    right: parent.right
                    margins: 10
                }
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: "<b>Window ID:</b> " + wid
                renderType: Text.RichText
            }

            MouseArea {
                anchors.fill: parent;

                onClicked: scaleFactor = (scaleFactor == 1) ? 3 : 1
            }
        }

        Component.onCompleted: {
            print('new item');
            controller.clientCountChanged()
            print('emit done');

        }
        //Behavior on height { PropertyAnimation { duration: 2000 } }
    }

    Slider {
        id: scaleSlider
        minimumValue: 1
        maximumValue: 5
        stepSize: 1.0
        anchors {
            right: parent.right
            left: parent.left
            bottom: clientsText.top
        }
    }

    Text {
        id: clientsText
        anchors {
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        text: "Connected clients: " + controller.clients.length
    }
    Component.onCompleted: {
        console.log("testserver.qml loaded.");
    }
}
