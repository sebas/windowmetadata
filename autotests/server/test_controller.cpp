/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/
// Qt
#include <QtTest/QtTest>

// Own
#include "debug.h"

// Server
#include "controller.h"
#include "metadataitem.h"
#include "servicewatcher_p.h"

// Client
#include "metadataclient.h"


namespace WindowMetadata {
namespace Server {

class TestController : public QObject
{
    Q_OBJECT
public:
    explicit TestController(QObject *parent = nullptr);

private Q_SLOTS:
    void init();
    void cleanup();

    void testAddRemove();

private:
    void testCleanup();
    void testControllerBeforeClient();

private:
    QList<WindowMetadata::Client::MetadataClient*> m_clients;
    Controller* m_controller = nullptr;

};

static auto s_serviceBaseName = QStringLiteral("org.kde.test.kwin.windowmetadata1.test_controller");

TestController::TestController(QObject *parent)
    : QObject(parent)
{
    qRegisterMetaType<WindowMetadata::Server::MetadataItemPointer>("MetadataItemPointer");
}

void TestController::init()
{

}

void TestController::cleanup()
{
    if (m_controller) {
        delete m_controller;
        m_controller = nullptr;
    }
    qDeleteAll(m_clients);
    m_clients.clear();
    ServiceWatcher::shutdown();
}

void TestController::testCleanup()
{
    QVERIFY(m_clients.isEmpty());
    auto c1 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    auto c2 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    auto c3 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);

    m_clients << c1 << c2 << c3;
    for (auto _c: m_clients) {
        QSignalSpy regSpy(_c, &WindowMetadata::Client::MetadataClient::registered);
        _c->registerService();
        QCOMPARE(regSpy.count(), 1);
    }

    ServiceWatcher::instance(s_serviceBaseName);
    QSignalSpy readySpy(ServiceWatcher::instance(), &ServiceWatcher::ready);
    QVERIFY(readySpy.wait(100));

    m_controller = new Controller(this);

    QSignalSpy clearSpy(m_controller, &Controller::itemRemoved);

    ServiceWatcher::shutdown();

    //QVERIFY(clearSpy.wait(200));
    QCOMPARE(clearSpy.count(), m_clients.count());

    // This will actually instantiate a new servicewatcher, we'll shutdown() it again laster...
    QCOMPARE(m_controller->items().count(), 0);
    delete m_controller;
    m_controller = nullptr;

    qDeleteAll(m_clients);
    m_clients.clear();
}

void TestController::testAddRemove()
{
    QVERIFY(m_clients.isEmpty());
    auto c1 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    auto c2 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    auto c3 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);

    m_clients << c1 << c2 << c3;
    for (auto _c: m_clients) {
        QSignalSpy regSpy(_c, &WindowMetadata::Client::MetadataClient::registered);
        _c->registerService();
        QCOMPARE(regSpy.count(), 1);
    }

    ServiceWatcher::instance(s_serviceBaseName);

    m_controller = new Controller(this);
    QSignalSpy readySpy(m_controller, &Controller::ready);
    QVERIFY(readySpy.wait(1000));

    qDebug() << "controller: " << m_controller->items().count();
    QSignalSpy addedSpy(m_controller, &Controller::itemAdded);
    QSignalSpy removedSpy(m_controller, &Controller::itemRemoved);

    QCOMPARE(m_controller->items().count(), m_clients.count());

    delete m_clients.last();
    m_clients.removeLast();

    QVERIFY(removedSpy.wait(2000));
    QCOMPARE(removedSpy.count(), 1);

    delete m_clients.last();
    m_clients.removeLast();

    QVERIFY(removedSpy.wait(2000));
    QCOMPARE(removedSpy.count(), 2);
    QCOMPARE(m_controller->items().count(), m_clients.count());

    delete m_clients.last();
    m_clients.removeLast();

    QVERIFY(removedSpy.wait(2000));
    QCOMPARE(removedSpy.count(), 3);
    QCOMPARE(m_controller->items().count(), m_clients.count());

    auto c4 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    c4->registerService();
    m_clients << c4;

    QVERIFY(addedSpy.wait(200));
    QCOMPARE(addedSpy.count(), 1);
    QCOMPARE(m_controller->items().count(), m_clients.count());

    auto c5 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    c5->registerService();
    auto c6 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    c6->registerService();
    m_clients << c5 << c6;

    QVERIFY(addedSpy.wait(200));
    QCOMPARE(addedSpy.count(), 2);
    QVERIFY(addedSpy.wait(200));
    QCOMPARE(addedSpy.count(), 3);
    QCOMPARE(m_controller->items().count(), m_clients.count());
}

void TestController::testControllerBeforeClient()
{
    QVERIFY(m_clients.isEmpty());
    m_controller = new Controller(this, s_serviceBaseName);

    QSignalSpy addedSpy(m_controller, &Controller::itemAdded);
    QSignalSpy removedSpy(m_controller, &Controller::itemRemoved);

    auto c1 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    auto c2 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    auto c3 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);

    m_clients << c1 << c2 << c3;
    for (auto _c: m_clients) {
        QSignalSpy regSpy(_c, &WindowMetadata::Client::MetadataClient::registered);
        _c->registerService();
        QCOMPARE(regSpy.count(), 1);
    }
    QVERIFY(addedSpy.wait(200));

    QCOMPARE(addedSpy.count(), m_clients.count());
    QCOMPARE(m_controller->items().count(), m_clients.count());

    delete m_clients.last();
    m_clients.removeLast();

    QVERIFY(removedSpy.wait(200));
    QCOMPARE(removedSpy.count(), 1);
    QCOMPARE(m_controller->items().count(), m_clients.count());

    auto c4 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    c4->registerService();
    m_clients << c4;
    QVERIFY(addedSpy.wait(200));
    QCOMPARE(addedSpy.count() - 1, m_clients.count()); // We unregistered one client earlier
    QCOMPARE(m_controller->items().count(), m_clients.count());
}

} }

QTEST_GUILESS_MAIN(WindowMetadata::Server::TestController)

#include "test_controller.moc"
