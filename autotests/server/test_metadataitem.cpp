/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/
// Qt
#include <QtTest/QtTest>
// Own
#include "metadataitem.h"
#include "debug.h"

namespace WindowMetadata {
namespace Server {

class TestMetadataItem : public QObject
{
    Q_OBJECT
public:
    explicit TestMetadataItem(QObject *parent = nullptr);
    void testProperties();
};

static auto s_serviceBaseName = QStringLiteral("org.kde.test.kwin.windowmetadata1.test_metadataitem");

TestMetadataItem::TestMetadataItem(QObject *parent)
    : QObject(parent)
{
}

void TestMetadataItem::testProperties()
{
    const QString sname = s_serviceBaseName + QStringLiteral("proptest0");
    auto wm = new MetadataItem(sname, this);

    QCOMPARE(wm->serviceName(), sname);
    QCOMPARE(wm->title(), QString());
    QSignalSpy titleSpy(wm, &MetadataItem::titleChanged);
    wm->setTitle(QStringLiteral("title changed"));
    QCOMPARE(titleSpy.count(), 1);
    QCOMPARE(wm->title(), QStringLiteral("title changed"));

    QCOMPARE(wm->pid(), 0);

    const int _wid = 587;
    QCOMPARE(wm->wid(), 0);
    QSignalSpy widSpy(wm, &MetadataItem::widChanged);
    wm->setWid(_wid);
    QCOMPARE(widSpy.count(), 1);
    QCOMPARE(wm->wid(), _wid);

    delete wm;
}

} }
QTEST_GUILESS_MAIN(WindowMetadata::Server::TestMetadataItem)
#include "test_metadataitem.moc"
