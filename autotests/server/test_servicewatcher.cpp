/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/
// Qt
#include <QtTest/QtTest>
// Own
#include "metadataclient.h"
#include "servicewatcher_p.h"
#include "debug.h"

namespace WindowMetadata {
namespace Server {

static auto s_serviceBaseName = QStringLiteral("org.kde.test.kwin.windowmetadata1.test_servicewatcher");


class TestServiceWatcher : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void testRegistration();
};


void TestServiceWatcher::testRegistration()
{
    auto c1 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    auto c2 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);
    auto c3 = new WindowMetadata::Client::MetadataClient(this, s_serviceBaseName);


    QList<WindowMetadata::Client::MetadataClient*> clients;
    clients << c1 << c2 << c3;
    for (auto _c: clients) {
        QSignalSpy regSpy(_c, &WindowMetadata::Client::MetadataClient::registered);
        _c->registerService();
        //QVERIFY(regSpy.wait(200));
        QCOMPARE(regSpy.count(), 1);
    }

    auto sw = new ServiceWatcher(this, s_serviceBaseName);
    QSignalSpy readySpy(sw, &ServiceWatcher::ready);

    QVERIFY(readySpy.wait());

    QCOMPARE(sw->registeredServices().count(), clients.count());

    QSignalSpy goneSpy(sw, &ServiceWatcher::serviceDisappeared);

    clients.removeAll(c1);
    delete c1;
    QVERIFY(goneSpy.wait());
    QCOMPARE(sw->registeredServices().count(), clients.count());

    clients.removeAll(c2);
    delete c2;
    QVERIFY(goneSpy.wait());
    QCOMPARE(sw->registeredServices().count(), clients.count());

    clients.removeAll(c3);
    delete c3;
    QVERIFY(goneSpy.wait());
    QCOMPARE(sw->registeredServices().count(), clients.count());

    QCOMPARE(sw->registeredServices().count(), 0);
    QCOMPARE(goneSpy.count(), 3);

}

} } // namespaces

QTEST_GUILESS_MAIN(WindowMetadata::Server::TestServiceWatcher)

#include "test_servicewatcher.moc"
