/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#include "metadataclient.h"
#include "metadataconnector_p.h"


namespace WindowMetadata {
namespace Client {

class MetadataClientPrivate {
public:
    MetadataClient *q;
    MetadataConnector *connector;
};

MetadataClient::MetadataClient(QObject* parent, const QString &serviceBaseName) :
    QObject(parent)
    , d(new MetadataClientPrivate)
{
    d->q = this;
    d->connector = new MetadataConnector(this, serviceBaseName);
    connect(d->connector, &MetadataConnector::objectRegistered, this, &MetadataClient::registered);
    connect(d->connector, &MetadataConnector::requestedSizeChanged, this, &MetadataClient::requestedSizeChanged);
    connect(d->connector, &MetadataConnector::titleChanged, this, &MetadataClient::titleChanged);
    connect(d->connector, &MetadataConnector::documentChanged, this, &MetadataClient::documentChanged);
    connect(d->connector, &MetadataConnector::widChanged, this, &MetadataClient::widChanged);
    connect(d->connector, &MetadataConnector::thumbnailRequested, this,
        [this](const QSize &thumbnailSize, int wid, const QString &filedescriptor) {
            Q_UNUSED(filedescriptor);
            emit thumbnailRequested(thumbnailSize, wid);
        }
    );
}

MetadataClient::~MetadataClient()
{
}

void MetadataClient::registerService()
{
    d->connector->registerService();
}

QSize MetadataClient::requestedSize() const
{
    return d->connector->requestedSize();
}

void MetadataClient::setThumbnail(const QImage &thumbnail, int windowId)
{
    d->connector->setThumbnail(thumbnail, windowId);
}

void MetadataClient::setDirty(int windowId)
{
    d->connector->setDirty(windowId);
}

QString MetadataClient::title() const
{
    return d->connector->title();
}

void MetadataClient::setTitle(const QString& title)
{
    return d->connector->setTitle(title);
}

int MetadataClient::wid() const
{
    return d->connector->wid();
}

QString MetadataClient::document() const
{
    return d->connector->document();
}

void MetadataClient::setDocument(const QString &document)
{
    return d->connector->setDocument(document);
}

void MetadataClient::setWid(int windowId)
{
    return d->connector->setWid(windowId);
}


} } // namespaces
