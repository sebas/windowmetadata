/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#include "metadataconnector_p.h"
#include "../protocolkeys.h"
#include "debug.h"

#include "windowmetadata1adaptor.h"

#include <QDBusMessage>

namespace WindowMetadata {
namespace Client {

    static auto s_defaultServiceBaseName = QStringLiteral("org.kde.kwin.windowmetadata1");

class MetadataConnectorPrivate {
public:
    MetadataConnector *q;

    void setDBusNames();
    void registerService();

    QString serviceName;
    QString objectName;
    QString serviceBaseName;
    Windowmetadata1Adaptor *adaptor = nullptr;

    QString filedescriptor;
    QImage thumbnail;
    QSize requestedSize;
    QVariantMap clientData;
};

MetadataConnector::MetadataConnector(QObject* parent, const QString &serviceBaseName) :
    QObject(parent)
    , d(new MetadataConnectorPrivate)
{
    d->q = this;
    d->clientData[KEY_PID] = qApp->applicationPid();
    d->clientData[KEY_WID] = 0;
    d->serviceBaseName = serviceBaseName;
    if (d->serviceBaseName.isEmpty()) {
        d->serviceBaseName = s_defaultServiceBaseName;
    }
    d->setDBusNames();
}

MetadataConnector::~MetadataConnector()
{
    if (!QDBusConnection::sessionBus().unregisterService(d->serviceName)) {
//         qCDebug(WMD) << "failed to unregisterService" << d->serviceName;
//         qCDebug(WMD) << "   Error:" << QDBusConnection::sessionBus().lastError().message();
//         qCDebug(WMD) << "failed to unregisterService" << d->serviceName;

    }
}

void MetadataConnectorPrivate::setDBusNames()
{
    QString _name = QUuid::createUuid().toString();
    _name = _name.remove(QStringLiteral("-"));
    _name = _name.remove(QStringLiteral("{"));
    _name = _name.remove(QStringLiteral("}"));

    serviceName = QString("%1.client_%2").arg(serviceBaseName, _name);
    objectName = QString("/org/kde/kwin/WindowMetadata1/client_%2").arg(_name);
}

void MetadataConnector::registerService()
{
    d->registerService();
}

void MetadataConnectorPrivate::registerService()
{
    if (!adaptor) {
        adaptor = new Windowmetadata1Adaptor(q);
        QObject::connect(q, &MetadataConnector::thumbnailUpdated, adaptor, &Windowmetadata1Adaptor::thumbnailUpdated);
    }

    if (!QDBusConnection::sessionBus().registerService(serviceName)) {
        qCWarning(WMD) << ")-: Could not register service" << serviceName;
        return;
    }

    if (!QDBusConnection::sessionBus().registerObject(objectName, q)) {
        qCWarning(WMD) << ")-: Could not register object." << objectName;
        return;

    }
    qCDebug(WMD) << "(-: Service registered." << serviceName << endl << objectName;
    Q_EMIT q->objectRegistered(serviceName);
}

void MetadataConnector::requestUpdate()
{
    qCDebug(WMD) << "request" << d->clientData;
    Q_EMIT dataUpdated(d->clientData);
}

void MetadataConnector::requestThumbnail(int width, int height, int wid, const QString& filedescriptor)
{
    QSize thumbnailSize(width, height);
    qCDebug(WMD) << "Thumbnail requested" << thumbnailSize << wid << filedescriptor;
    d->filedescriptor = filedescriptor;
    if (d->requestedSize != thumbnailSize) {
        d->requestedSize = thumbnailSize;
        Q_EMIT requestedSizeChanged();
    }
    Q_EMIT thumbnailRequested(thumbnailSize, wid, filedescriptor);
}

QSize MetadataConnector::requestedSize() const
{
    return d->requestedSize;
}

void MetadataConnector::setThumbnail(const QImage &thumbnail, int windowId)
{
    if (d->filedescriptor.isEmpty()) {
        qCDebug(WMD) << "Don't know where to save to.";
        return;
    }
    auto _size = thumbnail.size();
    if (_size != requestedSize()) {
        qCWarning(WMD) << "thumbnail size wrong! is: " << _size << ", but should be " << requestedSize();
        d->thumbnail = thumbnail.scaled(requestedSize());
    } else {
         d->thumbnail = thumbnail;
    }

    // write image
    if (d->thumbnail.save(d->filedescriptor, "PNG")) {
        //qCDebug(WMD) << "Thumb saved to" << d->filedescriptor << d->thumbnail.size();
        sendThumbnailUpdated(d->requestedSize, windowId);
        d->filedescriptor.clear();
    } else {
        qWarning(WMD) << "Could not save thumbnail to filedescriptor: " << d->filedescriptor;
    }
}

void MetadataConnector::sendThumbnailUpdated(const QSize &size, int windowId)
{
    QVariantMap args;
    args[KEY_HEIGHT] = size.height();
    args[KEY_WIDTH] = size.width();
    args[KEY_WID] = windowId;
    emit thumbnailUpdated(args);
}

void MetadataConnector::setDirty(int windowId)
{
    QVariantMap args;
    args[KEY_WID] = windowId;
    emit thumbnailDirty(args);
}

QString MetadataConnector::document() const
{
    return d->clientData[KEY_DOCUMENT].toString();
}

void MetadataConnector::setDocument(const QString &document)
{
    if (d->clientData[KEY_DOCUMENT] != document) {
        d->clientData[KEY_DOCUMENT] = document;
        if (d->adaptor) {
            Q_EMIT dataUpdated(d->clientData);
        }
        Q_EMIT documentChanged();
    }
}

QString MetadataConnector::title() const
{
    return d->clientData[KEY_TITLE].toString();
}

void MetadataConnector::setTitle(const QString& title)
{
    if (d->clientData[KEY_TITLE] != title) {
        d->clientData[KEY_TITLE] = title;
        if (d->adaptor) {
            Q_EMIT dataUpdated(d->clientData);
        }
        Q_EMIT titleChanged();
    }
}

int MetadataConnector::wid() const
{
    return d->clientData[KEY_WID].toInt();
}

void MetadataConnector::setWid(int windowId)
{
    if (windowId != d->clientData[KEY_WID].toInt() && windowId != -1) {
        d->clientData[KEY_WID] = windowId;
        // send data and emit "local" signal
        if (d->adaptor) {
            Q_EMIT dataUpdated(d->clientData);
        }
        Q_EMIT widChanged();
    }
}


} } // namespaces
