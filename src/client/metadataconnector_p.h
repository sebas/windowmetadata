/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#ifndef METADATA_CONNECTOR_H
#define METADATA_CONNECTOR_H

#include <QImage>
#include <QObject>

#include <WindowMetadata/Client/windowmetadata_export.h>

namespace WindowMetadata {
namespace Client {

class MetadataConnectorPrivate;

class MetadataConnector : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QSize requestedSize READ requestedSize NOTIFY requestedSizeChanged)
    Q_PROPERTY(int wid READ wid WRITE setWid NOTIFY widChanged)

    public:
        MetadataConnector(QObject* parent = 0, const QString &serviceBaseName = QString());
        ~MetadataConnector();

        void registerService();

        void setThumbnail(const QImage &thumbnail, int windowId = -1);
        void sendThumbnailUpdated(const QSize &size, int windowId);
        void setDirty(int windowId = -1);

        QString document() const;
        void setDocument(const QString &document);

        QString title() const;
        void setTitle(const QString &title);

        int wid() const;
        void setWid(int windowId);

        QSize requestedSize() const;

        Q_SCRIPTABLE void requestThumbnail(int width, int height, int wid, const QString &filedescriptor);
        Q_SCRIPTABLE void requestUpdate();


    Q_SIGNALS:
        void commit();
        void objectRegistered(const QString &service);

        void updateRequested();

        void thumbnailRequested(const QSize &thumbnailSize, int wid, const QString &filedescriptor);
        void thumbnailDirty(const QVariantMap &in0);
        void thumbnailUpdated(const QVariantMap &in0);
        void dataUpdated(const QVariantMap &in0);
        void requestedSizeChanged();
        void documentChanged();
        void titleChanged();
        void widChanged();

    private:
        QScopedPointer<MetadataConnectorPrivate> d;
};

} } // namespaces

#endif // METADATA_CLIENT_H
