/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#include "declarativeclient.h"
#include <metadataclient.h>

#include "debug.h"

#include <QOffscreenSurface>
#include <QOpenGLFramebufferObject>
#include <QOpenGLContext>
#include <QQuickItemGrabResult>
#include <QQuickRenderControl>
#include <QQuickWindow>
#include <QSharedPointer>
#include <QPointer>
#include <QTimer>

namespace WindowMetadata {
namespace Client {

using namespace WindowMetadata::Client;

class DeclarativeClientPrivate {
public:
    DeclarativeClient *q;
    MetadataClient *client = nullptr;
    QQuickItem *item = nullptr;
    QString document;
    QString title;

    QQuickRenderControl *renderControl = nullptr;
    QScopedPointer<QOpenGLContext> context;
    QScopedPointer<QOffscreenSurface> offscreenSurface;
    QScopedPointer<QOpenGLFramebufferObject> fbo;
    QImage buffer;
    QPointer<QQuickWindow> view;

    void setupOffscreenRenderer();
    void renderThumbnail();
    void cleanup();
};

DeclarativeClient::DeclarativeClient(QObject* parent) :
    QObject(parent),
    d(new DeclarativeClientPrivate)
{
    d->q = this;
    d->setupOffscreenRenderer();
}

DeclarativeClient::~DeclarativeClient()
{
    d->cleanup();
}

void DeclarativeClientPrivate::cleanup()
{
    if (context) {
        context->makeCurrent(offscreenSurface.data());

        delete renderControl;
        delete view.data();
        fbo.reset();
        delete item;
        delete view;

        context->doneCurrent();
    }
}

bool DeclarativeClient::registered() const
{
    return d->client != nullptr;
}

void DeclarativeClient::setRegistered(bool registerService)
{
    //qCWarning(WMD) << registerService;
    if (registerService && d->client == nullptr) {
        d->client = new MetadataClient(this);
        connect(d->client, &MetadataClient::thumbnailRequested,
            [this] (const QSize &thumbnailSize, int wid) {
                if (d->view->geometry().size() != thumbnailSize) {
                    //resize(thumbnailSize);
                    d->view->resize(thumbnailSize);

                    // bit hackish, we allow more time for resizing when the image is larger
                    //const int timeout = thumbnailSize.width() + thumbnailSize.height();
                    const int timeout = 50;
                    QTimer::singleShot(timeout, this, [this, thumbnailSize] () {
                        if (thumbnailSize == d->client->requestedSize()) {
                            d->renderThumbnail();
                        }
                    });
                } else {
                    d->renderThumbnail();
                }
            }
        );
        connect(d->client, &MetadataClient::requestedSizeChanged, this,
            [this] () {
                //Q_EMIT requestedSizeChanged();
            }
        );
        d->client->setDocument(d->document);
        d->client->setTitle(d->title);
        // TODO connect thumbnailChanged() as well
        d->client->registerService();
        Q_EMIT registeredChanged();
    } else if (d->client != nullptr) {
        delete d->client;
        d->client = nullptr;
        Q_EMIT registeredChanged();
    }
}

QSize DeclarativeClient::requestedSize() const
{
    if (d->client) {
        return d->client->requestedSize();
    }
    return QSize();
}

QQuickItem* DeclarativeClient::thumbnail() const
{
    return d->item;
}

void DeclarativeClient::setThumbnail(QQuickItem* item)
{
    if (d->item != item) {
        d->item = item;

        //d->item->setVisible(true);
        auto visualParent = property("visualParent");
        if (visualParent.isValid()) {
            d->item->setParentItem(visualParent.value<QQuickItem*>());
            visualParent.value<QQuickItem*>()->setProperty("drawBackground", false);
            qCDebug(WMD) << "has visual parent";
        } else {
            d->item->setParentItem(d->view->contentItem());
            qCDebug(WMD) << "no visual parent";
        }

        // connect destroyed
        connect(item, &QObject::destroyed, this,
            [this] () {
                d->item = nullptr;
            }
        );
        Q_EMIT thumbnailChanged();
    }
}

void DeclarativeClientPrivate::setupOffscreenRenderer()
{
    // first create the context
    QSurfaceFormat format;
    format.setDepthBufferSize(16);
    format.setStencilBufferSize(8);
    context.reset(new QOpenGLContext);
    context->setFormat(format);
    context->create();
    // and the offscreen surface
    offscreenSurface.reset(new QOffscreenSurface);
    offscreenSurface->setFormat(context->format());
    offscreenSurface->create();

    renderControl = new QQuickRenderControl();
    view = new QQuickWindow(renderControl);
    view->setFlags(Qt::BypassWindowManagerHint);
    view->setColor(Qt::transparent);
    if (client) {
        view->resize(client->requestedSize());
    }
    view->setVisible(true);

    context->makeCurrent(offscreenSurface.data());
    renderControl->initialize(context.data());
    context->doneCurrent();
    view->setVisible(false);
}

void DeclarativeClientPrivate::renderThumbnail()
{
    // if we're not connected, don't bother
    if (!client) {
        return;
    }
    //view->setVisible(true);
    view->update();

    if (!context->makeCurrent(offscreenSurface.data())) {
        return;
    }
    if (fbo.isNull() || fbo->size() != view->size()) {
        fbo.reset(new QOpenGLFramebufferObject(view->size(), QOpenGLFramebufferObject::CombinedDepthStencil));
        if (!fbo->isValid()) {
            qCWarning(WMD) << "Creating FBO as render target failed";
            fbo.reset();
            return;
        }
    }
    view->setRenderTarget(fbo.data());
    renderControl->polishItems();
    renderControl->sync();
    renderControl->render();

    view->resetOpenGLState();
    buffer = fbo->toImage();
    QOpenGLFramebufferObject::bindDefault();
    //view->setVisible(false);

    qCDebug(WMD) << "OffscreenImage: " << buffer.size();
    client->setThumbnail(buffer);
}

QString DeclarativeClient::document() const
{
    return d->document;
}

void DeclarativeClient::setDocument(const QString& document)
{
    if (document != d->document) {
        d->document = document;
        if (d->client) {
            d->client->setDocument(document);
        }
        Q_EMIT documentChanged();
    }
}

QString DeclarativeClient::title() const
{
    return d->title;
}

void DeclarativeClient::setTitle(const QString& title)
{
    if (title != d->title) {
        d->title = title;
        if (d->client) {
            d->client->setTitle(title);
        }
        Q_EMIT titleChanged();
    }
}

void DeclarativeClient::setDirty()
{
    if (d->client) {
        qCDebug(WMD) << "set dirty";
        d->client->setDirty();
    }
}

} } // namespaces
