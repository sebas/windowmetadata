/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#ifndef WINDOWMETADATA_DECLARATIVECLIENT_H
#define WINDOWMETADATA_DECLARATIVECLIENT_H

#include <QObject>
#include <QQuickItem>
#include <QQuickWindow>

#include <WindowMetadata/Client/windowmetadata_export.h>

namespace WindowMetadata {
namespace Client {

class DeclarativeClientPrivate;

class WINDOWMETADATACLIENT_EXPORT DeclarativeClient : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool registered READ registered WRITE setRegistered NOTIFY registeredChanged)
    Q_PROPERTY(QSize requestedSize READ requestedSize NOTIFY requestedSizeChanged)
    Q_PROPERTY(QQuickItem* thumbnail READ thumbnail WRITE setThumbnail NOTIFY thumbnailChanged)
    Q_PROPERTY(QString document READ document WRITE setDocument NOTIFY documentChanged)
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)

    public:
        explicit DeclarativeClient(QObject* parent = nullptr);
        virtual ~DeclarativeClient();

        bool registered() const;
        void setRegistered(bool registerService);

        QSize requestedSize() const;

        QQuickItem* thumbnail() const;
        void setThumbnail(QQuickItem* item);

        QString document() const;
        void setDocument(const QString& document);

        QString title() const;
        void setTitle(const QString& title);

        Q_INVOKABLE void setDirty();

    Q_SIGNALS:
        void registeredChanged();
        void requestedSizeChanged();
        void thumbnailChanged();
        void documentChanged();
        void titleChanged();

    private:
        QScopedPointer<DeclarativeClientPrivate> d;
};

}} // namespaces

#endif // WINDOWMETADATA_DECLARATIVECLIENT_H
