/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#include "declarativecontroller.h"
#include <controller.h>
#include <metadataitem.h>

#include "debug.h"

#include <QSharedPointer>
#include <QTimer>

namespace WindowMetadata {
namespace Server {

using namespace WindowMetadata::Server;

class DeclarativeControllerPrivate {
public:
    DeclarativeController *q;
    Controller *controller = nullptr;
    QSize size;
    QList<MetadataItem*> items;
};

DeclarativeController::DeclarativeController(QObject* parent) :
    QObject(parent),
    d(new DeclarativeControllerPrivate)
{
    d->q = this;
    auto serviceBaseName = QString::fromLatin1(qgetenv("WINDOWMETADATA_SERVICEBASENAME"));

    d->controller = new Controller(this, serviceBaseName);
    d->size = QSize(64, 64);
    connect(d->controller, &Controller::ready, this, [this] () {

        for (auto item : d->controller->items()) {
            d->items.append(item.data());
            //item->requestThumbnail(d->size, -1);
        }
        if (d->items.count()) {
            Q_EMIT clientsChanged();
        }
        connect(d->controller, &Controller::itemAdded, this, &DeclarativeController::itemAdded);
        connect(d->controller, &Controller::itemRemoved, this, &DeclarativeController::itemRemoved);
    });

    //connect(d->controller, &Controller::itemAdded, this, &DeclarativeController::clientsChanged);
    //connect(d->controller, &Controller::itemAdded, this, &DeclarativeController::requestAllThumbnail);
//     connect(ServiceWatcher::instance(d->serviceBaseName), &ServiceWatcher::itemAdded, this, &DeclarativeController::itemAdded);
//     connect(ServiceWatcher::instance(d->serviceBaseName), &ServiceWatcher::itemRemoved, this, &DeclarativeController::itemRemoved);
}

DeclarativeController::~DeclarativeController()
{
}

void DeclarativeController::itemAdded(MetadataItemPointer item)
{
    d->items.append(item.data());
    Q_EMIT clientsChanged();
}

void DeclarativeController::itemRemoved(MetadataItemPointer item)
{
    d->items.removeAll(item.data());
    Q_EMIT clientsChanged();
}

QQmlListProperty<WindowMetadata::Server::MetadataItem> DeclarativeController::clients()
{
    QQmlListProperty<WindowMetadata::Server::MetadataItem> lst(this, d->items);
    return lst;
}

} } // namespaces

