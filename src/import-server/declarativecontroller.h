/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#ifndef WINDOWMETADATA_DECLARATIVECONTROLLER_H
#define WINDOWMETADATA_DECLARATIVECONTROLLER_H

#include <QObject>
#include <QQmlListProperty>

#include <WindowMetadata/Server/windowmetadata_export.h>
#include <metadataitem.h>

namespace WindowMetadata {
namespace Server {

class DeclarativeControllerPrivate;

class WINDOWMETADATASERVER_EXPORT DeclarativeController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<WindowMetadata::Server::MetadataItem> clients READ clients NOTIFY clientsChanged)

    public:
        explicit DeclarativeController(QObject* parent = nullptr);
        virtual ~DeclarativeController();

        QQmlListProperty<WindowMetadata::Server::MetadataItem> clients();

    public Q_SLOTS:
        void itemAdded(MetadataItemPointer item);
        void itemRemoved(MetadataItemPointer item);

    Q_SIGNALS:
        void clientsChanged();

    private:
        QScopedPointer<DeclarativeControllerPrivate> d;
};

}} // namespaces

#endif // WINDOWMETADATA_DECLARATIVECONTROLLER_H
