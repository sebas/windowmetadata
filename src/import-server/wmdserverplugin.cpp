/*
 *   Copyright 2012 by Sebastian Kügler <sebas@kde.org>

 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "wmdserverplugin.h"

#include <QtQml>
#include <QQmlEngine>

#include "declarativecontroller.h"
#include "wmimageprovider.h"
#include <metadataitem.h>

using namespace WindowMetadata::Server;

void WindowMetadataServerPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("org.kde.kwin.windowmetadata.server"));
    Q_UNUSED(engine)
    qRegisterMetaType<MetadataItem*>("MetadataItem");
    engine->addImageProvider(QStringLiteral("windowmetadata"), new ImageProvider);

}

void WindowMetadataServerPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("org.kde.kwin.windowmetadata.server"));
    qmlRegisterType<DeclarativeController>(uri, 1, 0, "Controller");
    qmlRegisterType<MetadataItem>(uri, 1, 0, "MetadataItem");
}


