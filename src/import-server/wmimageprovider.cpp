/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#include "wmimageprovider.h"
#include <metadataitem.h>
#include <controller.h>

#include "debug.h"

#include <QSharedPointer>
#include <QTimer>

namespace WindowMetadata {
namespace Server {

using namespace WindowMetadata::Server;

#include <QLatin1Literal>
#include <QSize>

ImageProvider::ImageProvider()
    : QQuickImageProvider(QQuickImageProvider::Image)
    , m_controller(new Controller(nullptr, QString::fromLatin1(qgetenv("WINDOWMETADATA_SERVICEBASENAME"))))
{
}

ImageProvider::~ImageProvider()
{
}

QImage ImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    //qCDebug(WMD) << id << size->width() << size->height() << requestedSize;
    Q_UNUSED(size)
    Q_UNUSED(requestedSize)
    QString itemName = id;
    itemName.remove(QStringLiteral("thumbnail/"));
    auto split = id.split(QStringLiteral("/"));
    if (split.count() != 3) {
        qCWarning(WMD) << "Invalid image url" << id;
        return QImage();
    }
    auto sname = split.at(1);
    auto ts = split.at(2);

    //qCDebug(WMD) << " path now: " << sname;
    for (auto item : m_controller->items()) {
        if (item->serviceName() == sname) {
            auto thumb = item->thumbnail();
            //qCDebug(WMD) << "FOUND ITEM!" << requestedSize << thumb.size() << " null? " << thumb.isNull();
            if (thumb.size() != requestedSize || thumb.isNull()) {
                if (item->thumbnailSize() != requestedSize) {
                    item->requestThumbnail(requestedSize, 666);
                }
            }
            if (!thumb.isNull() && thumb.size() != requestedSize) {
                return thumb.scaled(requestedSize);
            }
            //qCDebug(WMD) << "Happy days. :)";
            if (!thumb.isNull()) {
                return thumb;
            }
        }
    }
    QImage emptyImage(requestedSize, QImage::Format_ARGB32);
    emptyImage.fill(Qt::transparent);
    return emptyImage;
}

} } // namespaces

