set(SERVER_LIB_SRCS
    controller.cpp
    metadataitem.cpp
    servicewatcher.cpp
)
ecm_qt_declare_logging_category(SERVER_LIB_SRCS HEADER debug.h
                                               IDENTIFIER WMD
                                               CATEGORY_NAME kde.windowmetadata
                                               DEFAULT_SEVERITY Info)

qt5_add_dbus_interface(SERVER_LIB_SRCS ../org.kde.kwin.windowmetadata1.xml windowmetadata_interface )

add_library(KF5WindowMetadataServer ${SERVER_LIB_SRCS})
generate_export_header(KF5WindowMetadataServer
    BASE_NAME
        WindowMetadataServer
    EXPORT_FILE_NAME
        WindowMetadata/Server/windowmetadata_export.h
)


add_library(KF5::WindowMetadataServer ALIAS KF5WindowMetadataServer)

target_include_directories(KF5WindowMetadataServer INTERFACE "$<INSTALL_INTERFACE:${KF5_INCLUDE_INSTALL_DIR}/WindowMetadata/Server>")

target_link_libraries(KF5WindowMetadataServer
    PUBLIC
        Qt5::Gui
    PRIVATE
        Qt5::DBus
    )

if(IS_ABSOLUTE "${KF5_INCLUDE_INSTALL_DIR}")
  target_include_directories(KF5WindowMetadataServer INTERFACE "$<INSTALL_INTERFACE:${KF5_INCLUDE_INSTALL_DIR}>" )
else()
  target_include_directories(KF5WindowMetadataServer INTERFACE "$<INSTALL_INTERFACE:${CMAKE_INSTALL_PREFIX}/${KF5_INCLUDE_INSTALL_DIR}>" )
endif()

set_target_properties(KF5WindowMetadataServer PROPERTIES VERSION   ${WINDOWMETADATA_VERSION_STRING}
                                                 SOVERSION ${WINDOWMETADATA_SOVERSION}
                                                 EXPORT_NAME WindowMetadataServer
)

install(TARGETS KF5WindowMetadataServer EXPORT KF5WindowMetadataTargets ${KF5_INSTALL_TARGETS_DEFAULT_ARGS})

install(FILES
  ${CMAKE_CURRENT_BINARY_DIR}/WindowMetadata/Server/windowmetadata_export.h
    controller.h
  DESTINATION ${KF5_INCLUDE_INSTALL_DIR}/WindowMetadata/Server COMPONENT Devel
)

include(ECMGeneratePriFile)
ecm_generate_pri_file(BASE_NAME WindowMetadataServer LIB_NAME KF5WindowMetadataServer DEPS "core" FILENAME_VAR PRI_FILENAME INCLUDE_INSTALL_DIR ${KDE_INSTALL_INCLUDEDIR_KF5})
install(FILES ${PRI_FILENAME}
        DESTINATION ${ECM_MKSPECS_INSTALL_DIR})
