/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#include "controller.h"
#include "metadataitem.h"
#include "servicewatcher_p.h"
#include "debug.h"

#include <QSharedPointer>
#include <QTimer>

namespace WindowMetadata {
namespace Server {

class ControllerPrivate {
public:

    QString serviceBaseName;
    Controller *q;
};

Controller::Controller(QObject* parent, const QString &serviceBaseName) :
    QObject(parent),
    d(new ControllerPrivate)
{
    if (serviceBaseName.isEmpty()) {
        d->serviceBaseName = QStringLiteral("org.kde.kwin.windowmetadata1");
    } else {
        d->serviceBaseName = serviceBaseName;
    }
    d->q = this;
    init();
}

Controller::~Controller()
{
}

MetadataItemList Controller::items() const
{
    return ServiceWatcher::instance(d->serviceBaseName)->items();
}

void Controller::init()
{
    connect(ServiceWatcher::instance(d->serviceBaseName), &ServiceWatcher::ready, this, &Controller::ready);
    connect(ServiceWatcher::instance(d->serviceBaseName), &ServiceWatcher::itemAdded, this, &Controller::itemAdded);
    connect(ServiceWatcher::instance(d->serviceBaseName), &ServiceWatcher::itemRemoved, this, &Controller::itemRemoved);
//     connect(ServiceWatcher::instance(d->serviceBaseName), &ServiceWatcher::ready, this, [this] () {
//         connect(ServiceWatcher::instance(d->serviceBaseName), &ServiceWatcher::itemAdded, this, &Controller::itemAdded);
//         connect(ServiceWatcher::instance(d->serviceBaseName), &ServiceWatcher::itemRemoved, this, &Controller::itemRemoved);
//     });
}

} } // namespaces

