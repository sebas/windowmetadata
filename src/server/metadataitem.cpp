/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#include "metadataitem.h"
#include "debug.h"
#include "windowmetadata_interface.h"
#include "../protocolkeys.h"

#include <QDBusConnection>
#include <QDBusInterface>
#include <QCoreApplication>
#include <QSize>
#include <QTemporaryFile>

namespace WindowMetadata {
namespace Server {

class MetadataItemPrivate {
public:
    MetadataItem *q;

    QString serviceName;
    QString objectName;
    // store the temp file name
    QString thumbnailFd;
    // this only gets set once the thumbnail has been written out
    QString thumbnailPath;
    QString createThumbnailUrl() const;
    QString thumbnailUrl;
    QSize thumbnailSize;
    QImage thumbnail;
    QString document;
    QString title;
    qint64 pid = 0;
    qint64 wid = 0;
    bool ready = false;

    OrgKdeKwinWindowmetadata1Interface *interface = nullptr;

};

MetadataItem::MetadataItem(const QString &serviceName, QObject* parent) :
    QObject(parent),
    d(new MetadataItemPrivate)
{
    d->q = this;
    d->serviceName = serviceName;

    auto client_ = d->serviceName.split(QStringLiteral("."));
    d->objectName = QString(QStringLiteral("/org/kde/kwin/WindowMetadata1/%1")).arg(client_.last());
    d->thumbnailUrl = d->createThumbnailUrl();

    d->interface = new OrgKdeKwinWindowmetadata1Interface(serviceName, d->objectName, QDBusConnection::sessionBus(), this);

    QObject::connect(d->interface, &OrgKdeKwinWindowmetadata1Interface::thumbnailDirty, this, &MetadataItem::slotThumbnailDirty);
    QObject::connect(d->interface, &OrgKdeKwinWindowmetadata1Interface::thumbnailUpdated, this, &MetadataItem::emitThumbnailUpdated);
    QObject::connect(d->interface, &OrgKdeKwinWindowmetadata1Interface::dataUpdated, this, &MetadataItem::dataUpdated);
    requestData();
}

MetadataItem::~MetadataItem()
{
}

void MetadataItem::emitReady()
{
    if (!d->ready) {
        d->ready = true;
        Q_EMIT ready();
    }
}

void MetadataItem::dataUpdated(const QVariantMap &args)
{
    auto _pid = args[KEY_PID].toInt();
    d->pid = _pid;

    auto _d = args[KEY_DOCUMENT].toString();
    setDocument(_d);

    auto _t = args[KEY_TITLE].toString();
    setTitle(_t);

    auto _w = args[KEY_WID].toInt();
    setWid(_w);

    emitReady();
}

QString MetadataItem::serviceName() const
{
    return d->serviceName;
}

QString MetadataItem::document() const
{
    return d->document;
}

void MetadataItem::setDocument(const QString &document)
{
    if (d->document != document) {
        d->document = document;
        Q_EMIT documentChanged();
    }
}

QString MetadataItem::title() const
{
    return d->title;
}

void MetadataItem::setTitle(const QString& title)
{
    if (d->title != title) {
        d->title = title;
        Q_EMIT titleChanged();
    }
}

qint64 MetadataItem::pid() const
{
    return d->pid;
}

qint64 MetadataItem::wid() const
{
    return d->wid;
}

void MetadataItem::setWid(const qint64 wid)
{
    if (d->wid != wid) {
        d->wid = wid;
        Q_EMIT widChanged();
    }
}

QSize MetadataItem::thumbnailSize() const
{
    return d->thumbnailSize;
}

void MetadataItem::requestThumbnail(const QSize& thumbnailSize, int wid)
{
    d->thumbnailSize = thumbnailSize;
    QList<QVariant> args;
    d->wid = wid;

    QDir tmpDir(QDir::tempPath() + QStringLiteral("/windowmetadata1/thumbnail"));
    tmpDir.mkpath(tmpDir.path());
    QTemporaryFile tmpfile(tmpDir.path());
    tmpfile.open(); // needed, otherwise fileName()'s gonna be empty
    d->thumbnailFd = tmpfile.fileName();
    tmpfile.close();

    args << thumbnailSize.width() << thumbnailSize.height() << wid << d->thumbnailFd;
    //qCDebug(WMD) << "Requesting thumb: " << thumbnailSize << args;
    d->interface->callWithArgumentList(QDBus::NoBlock, QStringLiteral("requestThumbnail"), args);
}

void MetadataItem::requestData()
{
    QList<QVariant> args;
    //qCDebug(WMD) << "Requesting data: ";
    d->interface->callWithArgumentList(QDBus::NoBlock, QStringLiteral("requestUpdate"), args);
}

QString MetadataItemPrivate::createThumbnailUrl() const
{
    auto _ts = QDateTime::currentDateTime().toString(QStringLiteral("yyMMdd_hhmmss_zzz"));
    auto url = QStringLiteral("image://windowmetadata/thumbnail/%1/%2").arg(serviceName, _ts);
    return url;
}

void MetadataItem::slotThumbnailDirty(const QVariantMap &args)
{
    auto _wid = args[KEY_WID].toInt();
    requestThumbnail(d->thumbnailSize, _wid);
}

void MetadataItem::emitThumbnailUpdated(const QVariantMap &args)
{
    const auto _size = QSize(args[KEY_WIDTH].toInt(), args[KEY_HEIGHT].toInt());

    // Avoid a race condition where we get a thumbnail we're not interested in any more
    // When multiple thumbnail requests are made in short order, we only want to notify
    // on our last request made (indicated by its size)
    if (d->thumbnailSize != _size || d->thumbnailPath == d->thumbnailFd ) {
        return;
    }

    // a window id of -1 means that the app doesn't specify a window. The thumbnail is
    // used for all of the app's windows.
    const auto _wid = args[KEY_WID].toInt();
    if (_wid != -1 && _wid != d->wid) {
        return;
    }

    d->thumbnailPath = d->thumbnailFd;
    d->thumbnailUrl = d->createThumbnailUrl();
    if (!d->thumbnailFd.isEmpty()) {
        const auto thumb = QImage(d->thumbnailFd);
        if (thumb.size() != d->thumbnailSize) {
            d->thumbnail = thumb.scaled(d->thumbnailSize); // GRmbLz.
        } else {
            d->thumbnail = thumb;
        }
    }
    //qCDebug(WMD) << "Got thumbnail" << _size << d->serviceName;
    Q_EMIT thumbnailChanged();
}

QString MetadataItem::thumbnailUrl() const
{
    return d->thumbnailUrl;
}

QImage MetadataItem::thumbnail()
{
    return d->thumbnail;
}


} } // namespaces

