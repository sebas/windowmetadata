/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#ifndef WINDOWMETADATA_METADATAITEM_H
#define WINDOWMETADATA_METADATAITEM_H

#include <QImage>
#include <QObject>
#include <QSharedPointer>
#include <QScopedPointer>

#include <WindowMetadata/Server/windowmetadata_export.h>

namespace WindowMetadata {
namespace Server {

class MetadataItemPrivate;

class WINDOWMETADATASERVER_EXPORT MetadataItem : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString serviceName READ serviceName CONSTANT)
    Q_PROPERTY(QString document READ document NOTIFY documentChanged)
    Q_PROPERTY(QString title READ title NOTIFY titleChanged)
    Q_PROPERTY(QImage thumbnail READ thumbnail NOTIFY thumbnailChanged)
    Q_PROPERTY(QString thumbnailUrl READ thumbnailUrl NOTIFY thumbnailChanged)
    Q_PROPERTY(qint64 pid READ pid CONSTANT)
    Q_PROPERTY(qint64 wid READ wid NOTIFY widChanged)

    public:
        explicit MetadataItem(const QString &serviceName = QString(), QObject* parent = nullptr);
        virtual ~MetadataItem();

        QString serviceName() const;
        QString document() const;
        QString title() const;
        qint64 pid() const;
        qint64 wid() const;

        QImage thumbnail();
        QString thumbnailUrl() const;
        QSize thumbnailSize() const;

        Q_INVOKABLE void requestThumbnail(const QSize &thumbnailSize, int wid);

    Q_SIGNALS:
        void serviceNameChanged();
        void documentChanged();
        void titleChanged();
        void widChanged();
        void thumbnailChanged();
        void ready();

    private:
        friend class TestMetadataItem;

        void setDocument(const QString &document);
        void setTitle(const QString &title);
        void setWid(const qint64 wid);

        void dataUpdated(const QVariantMap &args);
        void slotThumbnailDirty(const QVariantMap &args);
        void emitThumbnailUpdated(const QVariantMap &args);
        void emitReady();
        void requestData();

        QScopedPointer<MetadataItemPrivate> d;
};

typedef QSharedPointer<MetadataItem> MetadataItemPointer;
typedef QList<MetadataItemPointer> MetadataItemList;

}} // namespaces

Q_DECLARE_METATYPE(WindowMetadata::Server::MetadataItemPointer)
Q_DECLARE_METATYPE(WindowMetadata::Server::MetadataItem*)

 #endif // WINDOWMETADATA_METADATAITEM_H
