/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#include "servicewatcher_p.h"
#include "metadataitem.h"
#include "debug.h"

#include <QDBusConnection>
#include <QDBusConnectionInterface>
#include <QDBusInterface>
#include <QDBusPendingCallWatcher>

namespace WindowMetadata {
namespace Server {

static auto s_defaultServiceBaseName = QStringLiteral("org.kde.kwin.windowmetadata1");
static auto s_objectname = QStringLiteral("/org/kde/kwin/WindowMetadata1");

ServiceWatcher* ServiceWatcher::s_instance = nullptr;

class ServiceWatcherPrivate {
public:
    ServiceWatcher *q;
    MetadataItemList items;
    QHash<QString, MetadataItemPointer> itemsByService;
    QDBusInterface *interface = nullptr;

    QStringList registeredServices;
    QString serviceBaseName = s_defaultServiceBaseName;
    QStringList initializingServices;
    bool ready = false;
};

ServiceWatcher::ServiceWatcher(QObject* parent, const QString &serviceBaseName) :
    QObject(parent)
    , d(new ServiceWatcherPrivate)
{
    d->q = this;
    if (!serviceBaseName.isEmpty()) {
        d->serviceBaseName = serviceBaseName;
    }
    init();
}

ServiceWatcher::~ServiceWatcher()
{
    for (const QString &s: d->itemsByService.keys()) {
        serviceUnregistered(s);
    }
}

ServiceWatcher* ServiceWatcher::instance(const QString &serviceBaseName)
{
    if (!s_instance) {
        s_instance = new ServiceWatcher(nullptr, serviceBaseName);
    }
    return s_instance;
}

void ServiceWatcher::shutdown()
{
    if (s_instance) {
        delete s_instance;
        s_instance = nullptr;
    }
}

void ServiceWatcher::init()
{
    QDBusPendingCall async = QDBusConnection::sessionBus().interface()->asyncCall(QStringLiteral("ListNames"));
    QDBusPendingCallWatcher *callWatcher = new QDBusPendingCallWatcher(async, this);
    QObject::connect(callWatcher, &QDBusPendingCallWatcher::finished, this,
            [this](QDBusPendingCallWatcher *callWatcher) {
                serviceNameFetchFinished(callWatcher, QDBusConnection::sessionBus());
            });

    auto connection = QDBusConnection::sessionBus();
    connect(connection.interface(), &QDBusConnectionInterface::serviceRegistered, this, &ServiceWatcher::serviceRegistered);
    connect(connection.interface(), &QDBusConnectionInterface::serviceUnregistered, this, &ServiceWatcher::serviceUnregistered);
}

void ServiceWatcher::serviceNameFetchFinished(QDBusPendingCallWatcher* watcher, const QDBusConnection &connection)
{
    QDBusPendingReply<QStringList> propsReply = *watcher;
    watcher->deleteLater();

    if (propsReply.isError()) {
        qCWarning(WMD) << "Could not get list of available D-Bus services";
    } else {
        foreach (const QString& serviceName, propsReply.value()) {
            serviceRegistered(serviceName);
        }
    }

    // QDBusServiceWatcher is not capable for watching wildcard services right now
    // See:
    // https://bugreports.qt.io/browse/QTBUG-51683
    // https://bugreports.qt.io/browse/QTBUG-33829
    connect(connection.interface(), &QDBusConnectionInterface::serviceOwnerChanged, this, &ServiceWatcher::serviceOwnerChanged);
}

void ServiceWatcher::serviceOwnerChanged(const QString &serviceName, const QString &oldOwner, const QString &newOwner)
{
    //qCDebug(WMD) << "serviceOwnerChanged" << serviceName << oldOwner << newOwner;
    if (oldOwner.isEmpty()) {
        serviceRegistered(serviceName);
    } else if (newOwner.isEmpty()) {
        serviceUnregistered(serviceName);
    }
}

void ServiceWatcher::serviceRegistered(const QString &service)
{
    if (service.startsWith(d->serviceBaseName) && !d->registeredServices.contains(service) &&  !d->initializingServices.contains(service) ) {
        d->initializingServices.append(service);

        MetadataItemPointer item(new MetadataItem(service, this));
        connect(item.data(), &MetadataItem::ready, this, [=](){
            d->items.append(item);

            d->itemsByService.insert(service, item);
            d->registeredServices.append(service);
            d->initializingServices.removeAll(service);
            Q_EMIT itemAdded(item);
            Q_EMIT serviceAppeared(service);
            if (d->initializingServices.isEmpty() && !d->ready) {
                d->ready = true;
                Q_EMIT ready();
            }
        });
    }
}

void ServiceWatcher::serviceUnregistered(const QString &service)
{
    if (service.startsWith(d->serviceBaseName)) {
        if (d->initializingServices.contains(service)) {
            d->initializingServices.removeAll(service);
        }
        if (d->registeredServices.contains(service)) {
            MetadataItemPointer item = d->itemsByService.value(service);
            d->items.removeAll(item);
            d->itemsByService.remove(service);
            d->registeredServices.removeAll(service);
            Q_EMIT itemRemoved(item);
            Q_EMIT serviceDisappeared(service);
            item.clear();
        }
    }
}

MetadataItemList ServiceWatcher::items() const
{
    return d->items;
}

QStringList ServiceWatcher::registeredServices() const
{
    return d->registeredServices;
}

} } // namespaces

