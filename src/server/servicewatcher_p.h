/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/


#ifndef WINDOWMETADATA_SERVICEWATCHER_H
#define WINDOWMETADATA_SERVICEWATCHER_H

#include <QObject>

#include <QDBusConnection>

#include <WindowMetadata/Server/windowmetadata_export.h>
#include "metadataitem.h"

class QDBusPendingCallWatcher;

namespace WindowMetadata {
namespace Server {

class ServiceWatcherPrivate;

class WINDOWMETADATASERVER_EXPORT ServiceWatcher : public QObject
{
    Q_OBJECT

    public:
        ~ServiceWatcher();
        static ServiceWatcher* instance(const QString &serviceBaseName = QString());
        static void shutdown();

        QStringList registeredServices() const;
        MetadataItemList items() const;

    Q_SIGNALS:
        void ready();
        void serviceAppeared(const QString &service);
        void serviceDisappeared(const QString &service);

        void itemAdded(MetadataItemPointer item);
        /** item may or may not be valid, don't de-ref! */
        void itemRemoved(MetadataItemPointer item);

    private Q_SLOTS:
        void serviceNameFetchFinished(QDBusPendingCallWatcher* watcher, const QDBusConnection &connection);
        void serviceOwnerChanged(const QString &serviceName, const QString &oldOwner, const QString &newOwner);
        void serviceRegistered(const QString &service);
        void serviceUnregistered(const QString &service);

    private:
        ServiceWatcher(QObject* parent = nullptr, const QString &serviceBaseName = QString());
        friend class ClientsModel;
        friend class TestServiceWatcher;

        QScopedPointer<ServiceWatcherPrivate> d;
        static ServiceWatcher* s_instance;

        void init();
};

}} // namespaces

#endif // WINDOWMETADATA_SERVICEWATCHER_H
