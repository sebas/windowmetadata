/*
*   Copyright 2016 by Sebastian Kügler <sebas@kde.org>
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU Library General Public License as
*   published by the Free Software Foundation; either version 2, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Library General Public License for more details
*
*   You should have received a copy of the GNU Library General Public
*   License along with this program; if not, write to the
*   Free Software Foundation, Inc.,
*   51 Franklin Street, Fifth Floor, Boston, MA  2.010-1301, USA.
*/

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2

import org.kde.kwin.windowmetadata.client 1.0

Rectangle {
    id: root
    opacity: 0.8
    color: "lightgrey"
    width: 600
    height: 400

    MetadataClient {
        id: client
        document: previewImg.source
        title: titleEdit.text
        thumbnail: Rectangle {
            id: previewItem
            anchors.fill: parent
            color: "black"
            Image {
                id: previewImg
                source: "testimage3.jpg"
                sourceSize: requestedSize
                anchors {
                    fill: parent
                    margins: 8
                }
            }
//          Seems we can't do this across OpenGL contexts... it hides the source item :(
//             ShaderEffectSource {
//                 //sourceItem: imageRow
//                 hideSource: false
//                 anchors {
//                     fill: parent
//                     margins: 4
//                 }
//             }
            Rectangle {
                id: overlayBackground
                height: overlaytext.paintedHeight * 1.4
                color: "black"
                opacity: 0.7
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
            }
            Text {
                id: overlaytext
                color: "lightgrey"
                text: "Dimension: " + parent.width + " x " + parent.height
                elide: Text.ElideLeft
                //font.bold: true
                anchors {
                    left: overlayBackground.left
                    leftMargin: overlaytext.paintedHeight / 2
                    right: overlayBackground.right
                    rightMargin: overlaytext.paintedHeight / 2
                    verticalCenter: overlayBackground.verticalCenter
                }
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 20

        Label {
            text: "WindowMetadata Client"
        }

        CheckBox {
            text: "Registered"
            checked: client.registered
            onClicked: client.registered = checked
        }

        TextEdit {
            id: titleEdit
            text: "Declarative Client"
        }
        TextEdit {
            id: documentEdit
            text: "file://home/sebas/Documents/Suunto_D4i_UserGuide_EN.pdf"
        }

//         ShaderEffectSource {
//             id: imShader
//             sourceItem: imageRow
//             width: parent.width
//             height: 100
//         }
        Item {
            width: parent.width
            height: 100
        }

        RowLayout {
            id: imageRow
            Rectangle {
                color: (client.document == i1.source) ? "lightblue" : "transparent"

                width: 160
                height: 120

                Image {
                    id: i1
                    source: "testimage2.jpg"
                    anchors {
                        fill: parent
                        margins: 8
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        client.document = i1.source;
                        client.setDirty();
                    }
                }
            }
            Rectangle {
                color: (client.document == i2.source) ? "lightblue" : "transparent"

                width: 160
                height: 120

                Image {
                    id: i2
                    source: "testimage1.jpg"
                    anchors {
                        fill: parent
                        margins: 8
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        client.document = i2.source;
                        client.setDirty();
                    }
                }
            }


        }
    }

    Component.onCompleted: {
        client.registered = true;
    }
}
