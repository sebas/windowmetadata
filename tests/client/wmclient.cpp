/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/

#include "wmclient.h"
#include "ui_wmclient.h"
#include "debug.h"

#include <QFileDialog>
#include <QTest>

#include <QPainter>


using namespace WindowMetadata::Client;

WMClient::WMClient(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WMClient)
{
    ui->setupUi(this);

    ui->widEdit->setText(QStringLiteral("666"));
    registerClient();

    connect(ui->title, &QLineEdit::textChanged, this, [=] (const QString &txt) {
        if (m_client) {
            qCDebug(WMD) << "title changed: " << txt;
            m_client->setTitle(txt);
        }
    });

    auto file1 = QFINDTESTDATA("testimage1.jpg");
    auto file2 = QFINDTESTDATA("testimage2.jpg");
    auto file3 = QFINDTESTDATA("testimage3.jpg");

    m_thumbFile = file1;
    ui->image1Radio->setChecked(true);

    QSize _s(200, 200);
    ui->image1Label->setPixmap(QPixmap::fromImage(QImage(file1).scaled(_s, Qt::KeepAspectRatio)));
    ui->image2Label->setPixmap(QPixmap::fromImage(QImage(file2).scaled(_s, Qt::KeepAspectRatio)));
    ui->image3Label->setPixmap(QPixmap::fromImage(QImage(file3).scaled(_s, Qt::KeepAspectRatio)));

    connect(ui->image1Radio, &QAbstractButton::clicked, this, [=] (bool checked) {
        if (checked) {
            m_thumbFile = file1;
            if (m_client) {
                m_client->setDirty();
            }
        }
    });

    connect(ui->image2Radio, &QAbstractButton::clicked, this, [=] (bool checked) {
        if (checked) {
            m_thumbFile = file2;
            if (m_client) {
                m_client->setDirty();
            }
        }
    });

    connect(ui->image3Radio, &QAbstractButton::clicked, this, [=] (bool checked) {
        if (checked) {
            m_thumbFile = file3;
            if (m_client) {
                m_client->setDirty();
            }
        }
    });

    // Register / unregister

    ui->registerButton->setEnabled(false);
    connect(ui->registerButton,  &QPushButton::clicked, this, [this] {
        ui->registerButton->setEnabled(false);
        ui->unregisterButton->setEnabled(true);
        registerClient();
    });
    connect(ui->unregisterButton,  &QPushButton::clicked, this, [this] {
        ui->registerButton->setEnabled(true);
        ui->unregisterButton->setEnabled(false);
        closeClient();
    });
    ui->widEdit->setText(QString::number(ui->widEdit->effectiveWinId()));
    connect(ui->widEdit, &QLineEdit::textChanged, this, [=] (const QString &txt) {
        if (m_client) {
            int _id = txt.toInt();
            qCDebug(WMD) << "wid input chagned: " << txt << _id;
            m_client->setWid(_id);
        }
    });
}

void WMClient::registerClient()
{
    m_client = new WindowMetadata::Client::MetadataClient(this);
    auto wid = ui->widEdit->text().toInt();
    qCDebug(WMD) << "WID " << wid;
    m_client->setWid(wid);
    m_client->setTitle(ui->title->text());

    connect(m_client, &MetadataClient::thumbnailRequested, this, &WMClient::thumbnailRequested);

    m_client->registerService();
}

void WMClient::closeClient()
{
    delete m_client;
    m_client = nullptr;
    ctr = 0;
}

void WMClient::updateThumbnail(const QImage &image)
{
    ui->imageLabel->setPixmap(QPixmap::fromImage(image));
}

void WMClient::thumbnailRequested(const QSize& thumbnailSize, int wid)
{
    ui->widEdit->setText(QString::number(wid));
    ui->widthLabel->setText(QString::number(thumbnailSize.width()));
    ui->heightLabel->setText(QString::number(thumbnailSize.height()));

    ctr++;

    QImage image = QImage(m_thumbFile).scaled(thumbnailSize, Qt::IgnoreAspectRatio);
    QPainter painter(&image);

    auto font = QGuiApplication::font();
    painter.setFont(font);
    painter.setPen(QPen(Qt::lightGray));
    auto txt = QStringLiteral("Size %1x%2").arg(QString::number(thumbnailSize.width()), QString::number(thumbnailSize.height()));
    painter.drawText(QPoint(10, thumbnailSize.height() - 10), txt);
    painter.drawText(QPoint(10, 20), QString::number(ctr));
    painter.end();

    m_client->setThumbnail(image, wid);
    updateThumbnail(image);
    qCDebug(WMD) << "New thumbnail set " << wid << txt << ctr;
}


WMClient::~WMClient()
{
    delete ui;
}
