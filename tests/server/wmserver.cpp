/************************************************************************
 *                                                                      *
 *   Copyright 2016 Sebastian Kügler <sebas@kde.org>                    *
 *                                                                      *
 *   This library is free software; you can redistribute it and/or      *
 *   modify it under the terms of the GNU Lesser General Public         *
 *   License as published by the Free Software Foundation; either       *
 *   version 2.1 of the License, or (at your option) version 3, or any  *
 *   later version accepted by the membership of KDE e.V. (or its       *
 *   successor approved by the membership of KDE e.V.), which shall     *
 *   act as a proxy defined in Section 6 of version 3 of the license.   *
 *                                                                      *
 *   This library is distributed in the hope that it will be useful,    *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 *   Lesser General Public License for more details.                    *
 *                                                                      *
 *   You should have received a copy of the GNU Lesser General Public   *
 *   License along with this library.  If not, see                      *
 *   <http://www.gnu.org/licenses/>.                                    *
 *                                                                      *
 ************************************************************************/

#include "wmserver.h"
#include "ui_wmserver.h"
#include "debug.h"

#include <controller.h>

using namespace WindowMetadata::Server;

WMServer::WMServer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WMServer)
{
    ui->setupUi(this);

    connect(ui->updateButton, &QPushButton::clicked, this, [this] {
        qDebug() << "button clicked";
        ui->titles->setText(QStringLiteral("titles..."));
    });
    auto _server = new Controller(this);
}

WMServer::~WMServer()
{
    delete ui;
}
